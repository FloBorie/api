const express = require('express')
const axios = require('axios')
const app = express()
const url = 'https://jsonplaceholder.typicode.com'

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

const port = process.env.port || 8080 

//GET ALL
app.get('/users', async function(req, res){
    try {
        const { data } = await axios.get(url+'/users');
        res.send(data)
        console.log('data');
    } 
    catch (error) {
        console.error(error);
    }
})

//GET ID
app.get('/users/:id', async function(req, res){
    try {
        const { data } = await axios.get(url+'/users/'+req.params.id);
        res.send(data)
        console.log(data);
    } 
    catch (error) {
        console.error(error);
    }
})

//POST
app.post('/users', async function(req, res){
    try {
        const { data } = await axios.post(url+'/users',req.body);
        res.send(data)
        console.log('tu as bien ajouté un user!');
    } 
    catch (error) {
        console.error(error);
    }
})

//PUT
app.put('/users/:id', async function(req,res){
    try {
        const { data } = await axios.put(url+'/users/'+req.params.id,req.body);
        res.send(data)
        console.log('tu as bien modifier un user!');
    } 
    catch (error) {
        console.error(error);
    }
})

//DELETE
app.delete('/users/:id', async function(req,res){
    try {
        const { data } = await axios.delete(url+'/users/'+req.params.id);
        res.send(data)
        console.log('tu as bien supprimer un user!');
    } 
    catch (error) {
        console.error(error);
    }
})

//afficher tous les posts d'un ID
app.get('/users/:id/posts', async function(req, res){
    try {
        const { data } = await axios.get(url+'/users/'+req.params.id+'/posts');
        res.send(data)
        console.log(data);
    } 
    catch (error) {
        console.error(error);
    }
})

//afficher tous les albums d'un ID
app.get('/users/:id/albums', async (req, res) => {
    try {
        const {data} = await axios.get('https://jsonplaceholder.typicode.com/users/' + req.params.id)
        const albums = await axios.get('https://jsonplaceholder.typicode.com/users/' + req.params.id + '/albums/')
        data.albums = albums.data
        for (let album of data.albums) {
            const photos = await axios.get('https://jsonplaceholder.typicode.com/albums/' + album.id + '/photos/')
            album.photos = photos.data
        }
        console.log(data)
        res.send(data)
    } catch (error) {
        res.send(error)
    }
})

app.listen(port)